<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name=viewport content="width=device-width, initial-scale=1">
  <title>Video Player</title>
  <link href="assets/styles/player.css?ver=d_20161216" rel="stylesheet" />
  <style>
    html, body, #mod_player {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>
<div id="mod_player"></div>
<script>
  window.VIDEO_INFO = {
    status: 0,
    vid: "ed58e61d-3540-4e87-bbc3-4ec64967978e",
    poster: "http://dn-djidl2.qbox.me/cloud/274f56f10b07db8297c0fe901f07300e/2.jpg?sign=a20d9bc0b3269795a6353cad7114604c&amp;t=594a0dbb",
    duration: 315,
    historyStart: 0,
    title: "Hello",
    formats: "mp4",
    defaultTrack: "EN",
    definitions: [
    ],
    tracks: [
        
    ]
  }
  
  if(window.VIDEO_INFO.poster){
	  window.VIDEO_INFO.poster = window.VIDEO_INFO.poster.replace('http://', "//");
	  window.VIDEO_INFO.poster = window.VIDEO_INFO.poster.replace('https://', "//");
  }  
  
  var videoDefinitions = JSON.parse('[\n\
{"type":"SD","text":"480p","src":"http://dn-djidl2.qbox.me/cloud/274f56f10b07db8297c0fe901f07300e/sd.mp4?sign=5de5cce8911fc381c18633ffd8aab8f9&t=594a0dbb"},\n\
{"type":"HD","text":"720p","src":"http://dn-djidl2.qbox.me/cloud/274f56f10b07db8297c0fe901f07300e/720.mp4?sign=1c87b89b5984b7f3523a7a1217af4e58&t=594a0dbb"},\n\
{"type":"FHD","text":"1080p","src":"http://dn-djidl2.qbox.me/cloud/274f56f10b07db8297c0fe901f07300e/1080.mp4?sign=57cae1a82d13f1b4970dce3670b36919&t=594a0dbb"}\n\
]');
  for (di in videoDefinitions){
	  if(videoDefinitions[di]["src"]){
		  videoDefinitions[di]["src"] = videoDefinitions[di]["src"].replace('http://', "//");
		  videoDefinitions[di]["src"] = videoDefinitions[di]["src"].replace('https://', "//");
	  }
  }
  
  var videoTracks = JSON.parse('[\n\
{"kind":"subtitles","srclang":"ES","label":"Spanish","src":"assets/subtitle/es/8d312ed8f148d5d24983371aa268c8ff.vtt"},\n\
{"kind":"subtitles","srclang":"DE","label":"German","src":"assets/subtitle/de/20170425/4d16af802bb0c0dba338a88fda6d47bb.vtt"},\n\
{"kind":"subtitles","srclang":"FR","label":"French","src":"assets/subtitle/fr/5fe7c756721f38325f6d1df3a8f5e74e.vtt"},\n\
{"kind":"subtitles","srclang":"KO","label":"Korean","src":"assets/subtitle/ko/0967ba97b711c5db52d5e8673e7524f4.vtt"},\n\
{"kind":"subtitles","srclang":"EN","label":"English","src":"assets/subtitle/en/32bf989d53debe3a2f49b9aeb09b15c6.vtt"}]');
  for (ci in videoTracks){
	  if(videoTracks[ci]["src"]){
		  videoTracks[ci]["src"] = videoTracks[ci]["src"].replace('http://', "//");
		  videoTracks[ci]["src"] = videoTracks[ci]["src"].replace('https://', "//");
	  }
  }
  
  window.VIDEO_INFO.definitions = videoDefinitions;
  window.VIDEO_INFO.tracks = videoTracks;
  
  
</script>
<div id="logger"></div>
<script src="assets/scripts/player.js?ver=d_20161216"></script>
<script>
  function getUrlParam(p, u) {
    u = u || document.location.toString()
    var reg = new RegExp('(^|&|\\\\?)' + p + '=([^&]*)(&|$|#)')
    var r = u.match(reg)
    return r ? r[2] : ''
  }

  window.onerror = function (message, source, lineno, colno, error) {
    console.log(message, source, lineno, colno, error)
  }
  
  function initPlayer() {
    window.VIDEO_INFO = window.VIDEO_INFO || {}
    window.VIDEO_INFO.vid = window.VIDEO_INFO.vid   
    window.VIDEO_INFO.poster = getUrlParam('poster') || window.VIDEO_INFO.poster 
    window.VIDEO_INFO.title = getUrlParam('title') || window.VIDEO_INFO.title
    window.VIDEO_INFO.defaultTrack = getUrlParam('language') || window.VIDEO_INFO.defaultTrack
    
    var autoplay = getUrlParam('autoplay')
    autoplay = (autoplay === '0' || autoplay === 'false') ? 0 : 1
    
    var params = {
              autoplay: autoplay,
              loop: getUrlParam('loop'),
              source: window.VIDEO_INFO
            }
            var disableControls = getUrlParam('disctrl')
            disableControls = (disableControls == 1 || disableControls === 'true') ? 1 : 0
            if (disableControls) {
              params.disabledChildren = [
                'controls',
                'rightClick'
              ]
            }
    var player = jxplayer('mod_player').setup(params)
    
    /* document.addEventListener("WeixinJSBridgeReady", function () {
          if (autoplay) {
            player.play()
          }
        }, false); */
    
    /* ;(function (reporter) {
    	    
    	    var getQuery = function (p, u) {
    	        u = u || location.href;
    	        var reg = new RegExp('[\?&#]' + p + '=([^&#]+)', 'gi'),
    	          matches = u.match(reg),
    	          strArr;
    	        if (matches && matches.length > 0) {
    	          strArr = (matches[matches.length - 1]).split('=');
    	          if (strArr && strArr.length > 1) {
    	            return strArr[1];
    	          }
    	          return '';
    	        }
    	        return '';
    	      }
    	    
    	      var isNeedReport = function () {
    	        var source = getQuery('dji_source')
    	        if (source === 'dji' && vid === '3e87cadc-a60e-471b-bab0-fcf3be9cb4a6') {
    	          return false
    	        }
    	        return true
    	      }
    	
    	  try {
    	    var arr = [
    	      // 品牌故事
    	      '2cf5efa0-1500-4fc9-a535-b9ae39b46699',
    	      // 电影学堂
    	      '4eda0e41-a0e1-455c-ab4b-211b8feb8073'
    	    ]
    	    
    	    var vid = window.VIDEO_INFO.vid
    	    
    	    if (!isNeedReport()) {
    	          return
    	    }
    	    
    	    if (arr.indexOf && arr.indexOf(vid) < 0) {
    	      vid = 'other'
    	    }
    	    reporter.reportView(vid)
    	    player.on('firstplay', function () {
    	      reporter.reportTime(vid, 0)
    	    })
    	    player.on('time', function (time) {
    	      reporter.reportTime(vid, time)
    	    })
    	  } catch (e) {
    	  }
    	}(window.dji_player_ipinyou)); */
    
  }

  /* setTimeout(function () {
  console.log(jxplayer)
}, 3000); */
  
  document.addEventListener("DOMContentLoaded", initPlayer);
</script>

</body>
</html>